Advent of Code 2015 - Shawn Chowdhury
=====================================

My solutions for 2015's [Advent of Code](http://adventofcode.com).

It's really not necessary, but my goal is to figure out a project structure that allows me to add new solutions and test them without much work. 

Currently, each day is broken out into its own file/class to contain the input data\* and solutions to both parts of each day. I'm sure I'll come up with something better soon.

_\*Yeah I should probably put these in flatfiles but I already know how to fuckin do I/O that's really not the point of this >:T_
